# standard library
from pathlib import Path
from setuptools import setup, find_packages
from azi_pack import __about__

# Current working directory
HERE = Path(__file__).parent

with open(HERE / "requirements.txt") as f:
    requirements = [ line for line in f.read().splitlines() if not line.startswith(('#','-')) and len(line) ]

setup(name=__about__.__package_name__,
      py_modules=[__about__.__package_name__],
      packages=find_packages(),
      install_requires=requirements,
      #cli
      entry_points={ "console_scripts": [f"{__about__.__executable_name__} = azi_pack.__main__:main"] })