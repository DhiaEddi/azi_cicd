create virtual environment:

`python -m venv venv`

activate virtual environment: 

on windows: `venv\Scripts\activate`

on linux: `source venv/bin/activate`

install packages:

`pip install -r requirements.txt` or `python -m pip install -r requirements.txt`

build project and map local packages:

`python setup.py install`

Run tests: `coverage run -m pytest -vv`

Tests report: `coverage report` `coverage html`


To run the app as an executable run: `azi-app` or like a basic python script: `python azi_pack/__main__.py`

Test the app with by sending a get request to the following link: http://localhost:5000/get-capital?country_code=TN

docker run -p 5000:5000 registry.gitlab.com/dhiaeddi/azi_cicd/aziapp:1.0.0
