import sys
from typing import List
import time
import logging
from flask import Flask, request, jsonify
import json
import traceback
import argparse

from azi_pack.__about__ import __title__, __version__, __summary__
from azi_pack.countries import countries


gmt = time.gmtime()
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s',
    filename=f'azi_app_{gmt.tm_year}_{gmt.tm_mon}_{gmt.tm_mday}_{gmt.tm_hour}_{gmt.tm_min}.log',
    filemode='w'
)
logger = logging.getLogger(__name__)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
console_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
console_handler.setFormatter(console_formatter)
logger.addHandler(console_handler)

app = Flask(__name__)

# Load countries and capitals from static JSON file
logger.info("AZI app initialisation")

def find_capital(country_code=None):
    country_code = country_code.upper()

    if country_code in countries.keys():
        capital = countries[country_code]["capital"]
        return jsonify({"country_code": country_code, "capital": capital})
    else:
        return jsonify({"error": "Country code not found"}), 404  

@app.route('/get-capital', methods=['GET'])
def get_capital():
    try:
        country_code = request.args.get('country_code')
        if country_code is None:
            return jsonify({"error": "Please provide a 'country_code' parameter in the request"}), 400
        
        return find_capital(country_code)
    except Exception:
        logger.error(f"Unexpected error occured. Trace:{traceback.format_exc()}")
        return jsonify({"error": "Internal server error"}), 500



def main(argv: List[str] = None):

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=f"Check repository for documentation",
                                     description=f"{__title__} {__version__} - {__summary__}")
    parser.add_argument('--version', action='version', version=__version__)

    logger.info("AZI app started")
    app.run(host='0.0.0.0',port=5000,debug=True)


if __name__ == "__main__":
    main(sys.argv[1:])
