#! python3  # noqa E265

# standard library
import unittest

# 3rd party
from semver import VersionInfo
from validators import url

# project
from azi_pack import __about__


# ############################################################################
# ########## Classes #############
# ################################


class TestAbout(unittest.TestCase):
    """Test package metadata."""

    def test_metadata_types(self):
        """Test types."""
        # general
        self.assertIsInstance(__about__.__executable_name__, str)
        self.assertIsInstance(__about__.__package_name__, str)
        self.assertIsInstance(__about__.__summary__, str)
        self.assertIsInstance(__about__.__title__, str)
        self.assertIsInstance(__about__.__version__, str)


    def test_version_semver(self):
        """Test if version comply with semantic versioning."""
        self.assertTrue(VersionInfo.isvalid(__about__.__version__))


# ############################################################################
# ####### Stand-alone run ########
# ################################
if __name__ == "__main__":
    unittest.main()


